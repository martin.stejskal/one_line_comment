#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

if(sys.version_info[0] == 2):
  c_input = raw_input("Please type text\n >")
elif(sys.version_info[0] == 3):
  c_input = input("Please type text\n >")
else:
  print("Sorry, but this script support only python 2 and 3. You can try\n"
        "modify this script according to python 3 style. :(")
  
# Length of input text
i_in_text_length = len(c_input)

# How many "=" symbols will be before "| "
i_eq_pre = 0;

# How many "=" symbols will be after " |"
i_eq_post = 0;


# If string is longer than 73 characters, there will not be enough space to
# other characters -> write message
if(i_in_text_length > 73):
  print(" Sorry. Input text can not be longer than 73 characters :(")
  sys.exit()

# If we know input text length -> add needed symbols and characters
c_output = "# "

# Calculate how many "=" can be draw before "| " and after " |"
i_eq_pre = (78-6-i_in_text_length)>>1
i_eq_post = 78-6-i_in_text_length-i_eq_pre

for i in range(i_eq_pre):
  c_output = c_output + "="

# Add "| "
c_output = c_output + "| "

# Add text
c_output = c_output + c_input

# Add " |"
c_output = c_output + " |"

# Add rest of "="
for i in range(i_eq_post):
  c_output = c_output + "="

print("Input length: {0} Pre: {1} Post: {2}".format(i_in_text_length,
                                                    i_eq_pre,
                                                    i_eq_post))
print("Result:\n\n{0}\n".format(c_output))
