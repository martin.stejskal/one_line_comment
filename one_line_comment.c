/**
 * \file
 *
 * \brief Do nice ASCII 79 characters long comment for C style language
 *
 * 0) Compile application (make).\n
 * 1) Run application.\n
 * 2) Type text. Text should not be longer than 73 characters.\n
 * 3) Copy and paste result to your code.\n
 * 4) Profit!\n
 * \n
 * Using just stdio.h library, so compile should work on all platforms.\n
 *
 * Created 30.3.2014
 * Modified 30.10.2018
 *
 * \author Martin Stejskal
 * \version 1.2
 */
#include <stdio.h>      // Standard I/O

///\brief Define buffer size for standard input (keyboard typicaly)
#define MAX_INPUT_BUFFER        200

/**
 * \brief Main (and only) function
 * @return 0 if all right
 */
int main(void)
{
  // Input characters
  char c_input[MAX_INPUT_BUFFER];

  // Output buffer -> 79 characters + NULL
  char c_output[80];

  // Pointer to c_output
  char *p_c_output;
  p_c_output = &c_output[0];

  // Length of input text
  int i_in_text_length = 0;

  // How many "=" symbols will be before "| "
  int i_eq_pre = 0;

  // How many "=" symbols will be after " |"
  int i_eq_post = 0;

  // For iteration cycles
  int i;

  // Get input text to process
  printf("Please type text\n >");
  if(fgets(&c_input[0], MAX_INPUT_BUFFER , stdin) == NULL){
      printf("Empty string! Exiting...\n");
      return -1;
  }


  /* Let's find out, how long is input string. If will be longer than 74, there
   * will not be enough space to other characters -> return error
   */
  while(c_input[i_in_text_length] != '\n')
  {
    /* If input text length is longer than maximum, then return -1 (error).
     * Auto increase i_in_text_length after comparsion
     */
    if(i_in_text_length++ > 72)
    {
      printf("\n Sorry. Input text can not be longer than 72 characters :(\n");
      return -1;
    }
  }

  // If we know input text length -> add needed symbols and characters
  *(p_c_output++) = '/';
  *(p_c_output++) = '/';
  *(p_c_output++) = ' ';

  // Calculate how many "=" can be draw before "| " and after " |"
  i_eq_pre  = (77-6-i_in_text_length)>>1;
  i_eq_post = 77-6-i_in_text_length-i_eq_pre;

  for(i=i_eq_pre ; i > 0 ; i--)
  {
    *(p_c_output++) = '=';
  }

  // Add "| "
  *(p_c_output++) = '|';
  *(p_c_output++) = ' ';

  // Add text
  for(i=0 ; i < (i_in_text_length) ; i++)
  {
    *(p_c_output++) = c_input[i];
  }

  // Add " |"
  *(p_c_output++) = ' ';
  *(p_c_output++) = '|';

  // And add rest of "="
  for(i=i_eq_post ; i > 0 ; i--)
  {
    *(p_c_output++) = '=';
  }

  printf("Input length: %d Pre: %d Post: %d\n", i_in_text_length, i_eq_pre, i_eq_post);
  printf("Result:\n\n%s\n\n", c_output);

  // Program exit successfully
  return 0;
}
