# One line comment application

## Description
 * Do nice ASCII 79 characters long comment for C style language.
 * Using just stdio.h library, so compile should work on all platforms.
 * Do nice ASCII 79 characters long comment for Python style language
 
## Files
 * one_line_comment.c Source code
 * one_line_comment.py Source code

## Usage .c file
 * 0) Compile application (make).
 * 1) Run application.
 * 2) Type text. Text should not be longer than 72 characters.
 * 3) Copy and paste result to your .c/.h code.
 * 4) Profit!

## Usage .py file
 * 0) python one_line_comment.py
 * 1) Type text. Text should not be longer than 73 characters.
 * 2) Copy and paste result to your .py code.
 * 3) Profit!
